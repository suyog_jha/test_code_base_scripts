import snowflake.connector as sf
import mymodule as m

conn = sf.connect(user=m.username, password=m.password, account=m.account)

def execute_query(query):
    cursor = conn.cursor()
    cursor.execute(query)
    cursor.close()

try :
    sql = 'use {}'.format(m.database)
    execute_query(sql)

    sql = 'use warehouse {}'.format(m.warehouse)
    execute_query(sql)
    try:
        sql = 'alter warehouse {} resume'.format(m.warehouse)
        execute_query(sql)
    except:
        pass

    sql = 'select * FROM test_table'
    cur = conn.cursor()
    cur.execute(sql)
    for x in cur:
        print(x)
    cur.close()
except Exception as e:
    print(e)
finally:
    conn.close()
